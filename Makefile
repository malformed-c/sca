
CC=gcc
LIBS=-lncurses
OPTIONS=-0fast -DNDEBUG -Wall -Wextra -fanalyzer -std=gnu2x -fprofile-use $(LIBS)
DOPTIONS=-Og -g3 -Wall -Wextra -fanalyzer -std=gnu2x -pg -fprofile-generate $(LIBS)

all: sca

sca: main.c
	$(CC) $(DOPTIONS) -o sca main.c

run: all
	./sca

val: all
	valgrind ./sca

gdb:
	gdb-pwndbg ./sca

rel:


clean:
	rm -rf sca main.gcda gmon.out profile
